import SpeechRecognition from './lib/speech.js';
import Lexer from './lib/lexer.js';
import Referee from './lib/referee.js';
import World from './lib/world.js';
import Intro from './lib/intro.js';

var world = new World();
Intro.tavernScene(world);
var referee = new Referee(world);
var lexer = new Lexer();
var speech = new SpeechRecognition();

lexer.parse('There is a gun on the floor').then(intent => {

    referee.parseIntent(intent);
    lexer.parse('There is a dwarf').then(intent => {

        referee.parseIntent(intent);
        lexer.parse('I pick up the gun').then(intent => {
            
            referee.parseIntent(intent);
            lexer.parse('I shoot the dwarf').then(intent => {

                referee.parseIntent(intent);
            });

        });
    });
})

speech.start();
speech.subscribe((ev) => {
    var message = ev.results[0][0].transcript;
    return lexer.parse(message)
        .then(referee.parseIntent)
        .catch(console.error);
});

//var message = prompt('Next!');


