/* 
{
  "id": "c480b1ab-5927-482e-982e-5035523cb846",
  "timestamp": "2017-06-06T15:00:58.46Z",
  "lang": "en",
  "result": {
    "source": "agent",
    "resolvedQuery": "I grabbed the gun",
    "action": "",
    "actionIncomplete": false,
    "parameters": {
      "Actions": "Grab",
      "Actions1": "",
      "Characters": "",
      "Environment": "",
      "Objects": "gun",
      "Weather": ""
    },
    "contexts": [],
    "metadata": {
      "intentId": "86bb17d7-1977-4dd3-a815-6186f6838096",
      "webhookUsed": "false",
      "webhookForSlotFillingUsed": "false",
      "intentName": "User performs action"
    },
    "fulfillment": {
      "speech": "",
      "messages": [
        {
          "type": 0,
          "speech": ""
        }
      ]
    },
    "score": 0.58
  },
  "status": {
    "code": 200,
    "errorType": "success"
  },
  "sessionId": "234324234"
}
*/

const accessToken = "30557e6a9d014b509c1f562044c3e202";
const baseUrl = "https://api.api.ai/v1";

class Lexer {

    async parse(message) {

        return fetch(`${baseUrl}/query?v=20150910`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${accessToken}`
            },
            method: "POST",
            body: JSON.stringify({ query: message, lang: "en", sessionId: guidGenerator() })
        })
        .then(response => response.json())
        .then(json => json.result);
    }

}

function guidGenerator() {
    var S4 = function() {
       return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
    };
    return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
}

export default Lexer;