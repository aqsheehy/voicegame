import Entity from './entity.js';
import Players from './players.js';

class World {
    constructor(){
        this.anchor = new Entity("world");
        this.players = new Players();
    }
}

export default World;