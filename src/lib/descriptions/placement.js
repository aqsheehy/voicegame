import Entity from '../entity.js';

class Placement {
    static execute(intent, world) {
        var player = world.players.current;
        if (!player) return;

        var playerDirections = world.anchor.searchEntity(player);
        var environment = playerDirections[0].entity;

        if (intent.parameters.Objects)
            environment.link('contains', new Entity(intent.parameters.Objects));

        else if (intent.parameters.Characters)
            environment.link('contains', new Entity(intent.parameters.Characters))
    }
}

export default Placement;