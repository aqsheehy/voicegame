var SystemSpeech = 
    window.SpeechRecognition || 
    window.webkitSpeechRecognition || 
    window.mozSpeechRecognition || 
    window.msSpeechRecognition;

class SpeechRecognition {

    constructor() 
    {
        this.source = new SystemSpeech();
        this.source.lang = 'en-US';
        this.source.interimResults = false;
        this.source.continuous = true;
        this.source.maxAlternatives = 5;
    }

    start() {
        this.source.start();
    }

    stop() {
        this.source.stop();
    }

    subscribe(fn) {
        this.source.onresult = fn;
    }

}

export default SpeechRecognition;