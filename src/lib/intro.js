import Entity from './entity.js';

const DEFAULT_PLAYERS = 3;

class Intro {
    static tavernScene(world){

        var tavern = new Entity('tavern');
        world.anchor.link('contains', tavern);

        world.players.entities.forEach(player => {
            tavern.link('contains', player);
        });

    }
}

export default Intro;