class Shoot {

    static execute(intent, world) {

        var player = world.players.current;
        if (!player) return;

        var playerDirections = world.anchor.searchEntity(player);
        var environment = playerDirections[0].entity;

        // Can't shoot if not holding a gun-like object
        var gunDirections = player.search(edge => edge.key == 'holding' && edge.entity.name == 'gun', 1);
        if (!gunDirections) throw new Error('Cant shoot without a gun');

        // Gun is shot, does nothing.
        if (!intent.parameters.Characters) return;

        // Described target does not exist.
        var target = environment.search(edge => edge.key == 'contains' && edge.entity.name == intent.parameters.Characters, 2);
        if (!target) throw new Error('Character does not exist within this environment');

        target[target.length - 1].entity.destroy();
    }

}

export default Shoot;