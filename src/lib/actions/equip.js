class Equip {
    static execute(intent, world) {
        var player = world.players.current;
        if (!player) return;

        var playerDirections = world.anchor.searchEntity(player);
        var environment = playerDirections[0].entity;

        var target = environment.search(edge => 
            (edge.key == 'contains' || edge.key == 'holding' ) && 
            edge.entity.name == intent.parameters.Objects, 2);

        if (!target) throw new Error('Character does not exist within this environment');

        var connection = target[target.length - 1];
        connection.entity.severAll(edge => edge.key == connection.key);

        player.link('holding', connection.entity);
    }
}

export default Equip;