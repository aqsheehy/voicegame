import Entity from './entity.js';

const DEFAULT_PLAYERS = 3;

class Players {

    constructor(numPlayers) {
        this.entities = [];
        this.currentPlayerIndex = 0;
        this.numPlayers = numPlayers || DEFAULT_PLAYERS;
        for (var i = 1; i < this.numPlayers + 1; i++)
            this.entities.push(new Entity(`player ${i}`));
    }

    get current() {
        return this.entities[this.currentPlayerIndex];
    }

    increment(){
        this.currentPlayerIndex = (this.currentPlayerIndex + 1) % this.numPlayers;
    }


}

export default Players;