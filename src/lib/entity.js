/* 
    World
        \ Environments
            \ Characters
                \ Props
            \ Weather
            \ Props
                \ Characters    
                    \ Props
                \ Props
*/

class Edge {
    constructor(key, entity, direction) {
        this.key = key;
        this.entity = entity;
        this.direction = direction;
    }
}

const MAX_SEARCH_DEPTH = 20;
var ENTITY_ID = 0;
class Entity {

    constructor(name){
        this._name = name;
        this._id = ENTITY_ID;
        this._outbound = [];
        this._inbound = [];
        ENTITY_ID++;
    }

    get id() { return this._id; }
    get name() { return this._name; }    

    get edges() { return this.outbound.concat(this.inbound) }
    get outbound() { return this._outbound; }
    get inbound() { return this._inbound; }

    link(key, entity) {
        this.outbound.push(new Edge(key, entity, 'outbound'))
        entity.inbound.push(new Edge(key, this, 'inbound'));
    }

    unlink(key, entity) {
        var i1 = this.outbound.findIndex(edge => edge.key == key && edge.entity == entity);
        if (i1 > -1) this.outbound.splice(i1, 1);

        var i2 = entity.inbound.findIndex(edge => edge.key == key && edge.entity == this);
        if (i2 > -1) entity.inbound.splice(i2, 1);
    }

    unlinkAll(fn) {
        if (!fn) fn = function(edge) { return true; }
        this.outbound.filter(fn).forEach(edge => this.unlink(edge.key, edge.entity));
    }

    severAll(fn) {
        if (!fn) fn = function(edge) { return true }
        this.inbound.filter(fn).forEach(edge => edge.entity.unlink(edge.key, this));
    }
    
    destroy() {
        this.unlinkAll();
        this.severAll();
    }

    search(fn, searchDepth, searchSpace) {
        if (!searchDepth) searchDepth = MAX_SEARCH_DEPTH;
        if (!searchSpace) searchSpace = [];
        else if (searchSpace.length >= searchDepth) return null;

        for (var i in this.edges) {
            var edge = this.edges[i];
            if (searchSpace.indexOf(edge) > -1) continue;

            var chain = searchSpace.concat([ edge ]);
            if (fn(edge)) return chain;

            var innerSearch = edge.entity.search(fn, searchDepth, chain);
            if (innerSearch) return innerSearch;
        }

        return null;
    }

    searchId(entityId) {
        return this.search(edge => edge.entity.id == entityId);
    }

    searchEntity(entity){
        return this.searchId(entity.id);
    }
}

export { Edge }
export default Entity;