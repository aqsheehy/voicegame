import Entity from './entity.js';
import Players from './players.js';

import Equip from './actions/equip.js';
import Shoot from './actions/shoot.js';
import Placement from './descriptions/placement.js';

const ACTION_INTENT = 'User performs action';
const ENVIRONMENT_INTENT = 'Description of environment';

class Referee {

    constructor(world) {
        this.world = world;
    }

    parseIntent(intent) {
        switch (intent.metadata.intentName){
            case ACTION_INTENT:
                this.processAction(intent);
                break;
            case ENVIRONMENT_INTENT:
                this.processDescription(intent);
                break;
            default:
                return console.error('Unknown intent type!')
        }
    }

    processAction(intent){
        console.log('It is an action!');
        switch (intent.parameters.Actions){
            case 'Shoot': return Shoot.execute(intent, this.world);
            case 'Grab': return Equip.execute(intent, this.world);
            default: return null;
        }        
    }

    processDescription(intent){
        console.log('It is a description!');
        Placement.execute(intent, this.world);
    }

    processCondition(intent){
        console.log('It is a new win condition!');
    }

}

export default Referee;