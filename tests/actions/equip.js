import chai, { expect } from 'chai';
import World from '../../src/lib/world.js'
import Equip from '../../src/lib/actions/equip.js';
import Intro from '../../src/lib/intro.js';
import Entity from '../../src/lib/entity.js';

describe('Equip', () => {
    it('should do nothing if the target object does not exist', () => {

        var world = new World();
        Intro.tavernScene(world);

        var gun = new Entity('gun');
        expect(() => {
            Equip.execute({ Objects: gun.name }, world);
        })
        .to.throw();

    });

    it('should move the gun to being held by the current player, and still maintain all other relationship data', () => {
        var world = new World();
        Intro.tavernScene(world);

        var gun = new Entity('gun');
        var environment = world.anchor.outbound[0].entity;
        environment.link('contains', gun);

        var dust = new Entity('dust');
        dust.link('on', gun);

        Equip.execute({ Objects: gun.name }, world);
        expect(gun.inbound.length).to.equal(2);
        
        expect(gun.inbound[0].key).to.equal('on');
        expect(gun.inbound[1].key).to.equal('holding');
        expect(gun.inbound[1].entity).to.equal(world.players.current);
    });


});