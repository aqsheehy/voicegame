import chai, { expect } from 'chai';
import World from '../../src/lib/world.js'
import Shoot from '../../src/lib/actions/shoot.js';
import Intro from '../../src/lib/intro.js';
import Entity from '../../src/lib/entity.js';

describe('Shoot', () => {
    it('should require that the current player is holding a gun', () => {
        var world = new World();
        Intro.tavernScene(world);

        expect(() => {
            Shoot.execute({ }, world);
        })
        .to.throw();
    });

    it('should do nothing if the target character does not exist', () => {

        var world = new World();
        Intro.tavernScene(world);

        world.players.current.link('holding', new Entity('gun'));

        var goblin = new Entity('goblin');
        expect(() => {
            Shoot.execute({ Characters: goblin.name }, world);
        })
        .to.throw();

    });

    it('should remove all links to and from the target when shot', () => {

        var world = new World();
        Intro.tavernScene(world);

        world.players.current.link('holding', new Entity('gun'));

        var goblin = new Entity('goblin');
        var tavern = world.anchor.outbound[0].entity;
        tavern.link('contains', goblin);

        var tavernLinks = tavern.outbound.length;
        Shoot.execute({ Characters: goblin.name }, world);

        expect(goblin.inbound.length).to.equal(0);
        expect(tavern.outbound.length).to.equal(tavernLinks - 1);
    });
});