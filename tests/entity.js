import chai, { expect } from 'chai';
import Entity, { Edge } from '../src/lib/entity.js'

describe('Entity', () => {
  it('should allocate incrementing ids to entities', () => {
    var a = new Entity('a');
    var b = new Entity('b');
    var c = new Entity('c');
    expect(b.id).to.equal(a.id + 1);
    expect(c.id).to.equal(b.id + 1);
  });

  it('should be able to create bi-directional links to other Entities', () => {
    var home = new Entity('home');
    var person = new Entity('person');
    home.link('houses', person);
    expect(home.outbound[0].key).to.equal('houses');
    expect(home.outbound[0].entity).to.equal(person);
    expect(home.outbound[0].direction).to.equal('outbound');

    expect(person.inbound[0].key).to.equal('houses');
    expect(person.inbound[0].entity).to.equal(home);
    expect(person.inbound[0].direction).to.equal('inbound');
  });

  it('should be able to unlink links', () => {
    var home = new Entity('home');
    var person = new Entity('person');
    
    home.link('houses', person);
    home.link('test', person);
    home.unlink('houses', person);

    expect(home.outbound[0].key).to.equal('test');
    expect(home.outbound[0].entity).to.equal(person);
    expect(home.outbound[0].direction).to.equal('outbound');

    expect(person.inbound[0].key).to.equal('test');
    expect(person.inbound[0].entity).to.equal(home);
    expect(person.inbound[0].direction).to.equal('inbound');
  });

  it('should be able to locate an entity one hop away', () => {
    var home = new Entity('home');
    var person = new Entity('person');
    home.link('houses', person);

    var directions = home.searchId(person.id);
    expect(directions.length).to.equal(1);
    expect(directions[0].key).to.equal('houses');
    expect(directions[0].entity).to.equal(person);
    expect(directions[0].direction).to.equal('outbound');
  });

  it('should be able to locate an entity multiple hops away', () => {
    var home = new Entity('home');
    var person = new Entity('person');
    home.link('houses', person);
    var gun = new Entity('gun');
    person.link('holding', gun);

    var directions = home.searchId(gun.id);
    expect(directions.length).to.equal(2);
    expect(directions[0].key).to.equal('houses');
    expect(directions[0].entity).to.equal(person);
    expect(directions[0].direction).to.equal('outbound');

    expect(directions[1].key).to.equal('holding');
    expect(directions[1].entity).to.equal(gun);
    expect(directions[1].direction).to.equal('outbound');
  });

  it('should be able to locate an entity which requires backtracking', () => {
    var home = new Entity('home');
    var person = new Entity('person');
    home.link('houses', person);
    var gun = new Entity('gun');
    person.link('holding', gun);
    var dust = new Entity('dust');
    dust.link('on', gun);

    var directions = home.searchId(dust.id);

    expect(directions.length).to.equal(3);

    expect(directions[0].key).to.equal('houses');
    expect(directions[0].entity).to.equal(person);
    expect(directions[0].direction).to.equal('outbound');

    expect(directions[1].key).to.equal('holding');
    expect(directions[1].entity).to.equal(gun);
    expect(directions[1].direction).to.equal('outbound');    

    expect(directions[2].key).to.equal('on');
    expect(directions[2].entity).to.equal(dust);
    expect(directions[2].direction).to.equal('inbound');
  });

  it('should be able to search at a specified search depth', () => {
    var home = new Entity('home');
    var person = new Entity('person');
    home.link('houses', person);
    var gun = new Entity('gun');
    person.link('holding', gun);
    var dust = new Entity('dust');
    dust.link('on', gun);

    var directions = home.search(edge => edge.entity == dust, 2);
    expect(directions).to.equal(null);
  });

  it('should be able to unlink all edges on a query', () => {
    var home = new Entity('home');
    var person = new Entity('person');
    home.link('houses', person);
    var gun = new Entity('gun');
    person.link('holding', gun);
    var dust = new Entity('dust');
    dust.link('on', gun);

    home.unlinkAll();

    expect(home.outbound.length).to.equal(0);
    expect(person.inbound.length).to.equal(0);
  });

  it('should be able to destroy an entity and remove all links to it', () => {
    var home = new Entity('home');
    var person = new Entity('person');
    home.link('houses', person);
    var gun = new Entity('gun');
    person.link('holding', gun);
    var dust = new Entity('dust');
    dust.link('on', gun);

    gun.destroy();

    expect(dust.outbound.length).to.equal(0);
    expect(person.outbound.length).to.equal(0)
  });
});