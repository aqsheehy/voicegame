var gulp = require('gulp');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var browserify = require('browserify');

gulp.task('default', function () {

    browserify({ entries: 'src/index.js', debug: true })
        .transform(babelify)
        .bundle()
        .pipe(source('index.min.js'))
        .pipe(gulp.dest('./dist'));
        
    gulp.src('src/*.html')
        .pipe(gulp.dest('dist'));
});